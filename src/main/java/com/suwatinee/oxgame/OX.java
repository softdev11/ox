/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suwatinee.oxgame;

/**
 *
 * @author suwat
 */
import java.util.Scanner;
public class OX {
    static char winner = '-';
    static boolean isFinish =false;
    static int row,col;
     static Scanner kb =new Scanner(System.in);

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player='X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void ShowTable() {
         System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void ShowTurn() {
         System.out.println(player+" turn");

    }

    static void input() {
        while(true){
            System.out.println("Please input Row col:");
            row = kb.nextInt()-1;
            col = kb.nextInt()-1;
            if(table[row][col]=='-'){
                table[row][col]= player;
                break;
            }
            System.out.println("Error table at row and col is not Empty!!!");
        }
    }

    static void checkCol() {
         for(int row =0;row<3;row++){
            if(table [row][col] != player)
                return;
        }
        isFinish =true;
        winner = player;

    }

    static void checkRow() {
        for(int col =0;col<3;col++){
            if(table [row][col] != player)
                return;
        }
        isFinish =true;
        winner = player;


    }

    static void checkWin() {
        checkCol();
        checkRow();

    }

    static void switchPlayer() {
          if(player =='X'){
            player='O';
        }else{
            player ='X';
        }

    }

    static void showResult() {
         if(winner == '-'){
            System.out.println("Draw!!");
        }else{
            System.out.println(winner+" Win!!!");
        }

    }

    static void showBye() {
        System.out.println("Bye Bye ....");

    }

    public static void main(String[] args) {
        showWelcome();
        do{
        ShowTable();
        ShowTurn();
        input();
        checkWin();
        switchPlayer();
        }while(!isFinish);
        showResult();
        showBye();

    }
}
